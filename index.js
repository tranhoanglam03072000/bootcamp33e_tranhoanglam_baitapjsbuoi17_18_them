//===================== Bài 1==================================

// var ketqua = "";
// for (var i = 1; i <= 100; i++) {
//   if (i % 10 == 0) {
//     ketqua = ketqua + i + `<br>`;
//   } else {
//     ketqua = ketqua + i + " ";
//   }
// }
// document.getElementById("ketQuaInSo1").innerHTML = ketqua;
var ketQua1 = ``;
for (var i = 1; i <= 100; i += 10) {
  var ketQua2 = ``;
  for (var a = i; a <= i + 9; a++) {
    ketQua2 = ketQua2 + `<td>${a}</td>`;
  }
  ketQua1 = ketQua1 + `<tr>${ketQua2}</tr>`;
}

document.querySelector("#ketQuaInSo1").innerHTML = ketQua1;

//==========================Bài 2 ============================
// thêm số
var arrKetQuaThemSo2 = [];
themSo(
  "#btnThemSo2",
  "#nhapSo2",
  "#myForm2",
  arrKetQuaThemSo2,
  "#ketQuaThemSo2"
);
//Xóa số
xoaSo("#btnXoaSo2", arrKetQuaThemSo2, "#ketQuaThemSo2");
//Tính
document.querySelector("#btnDemSoNt2").onclick = function () {
  var dem = 0;
  for (var i = 0; i < arrKetQuaThemSo2.length; i++) {
    if (arrKetQuaThemSo2[i] == soNguyenTo(arrKetQuaThemSo2[i])) {
      dem++;
    }
  }
  document.querySelector(
    "#ketQuaDemSoNt2"
  ).innerHTML = `Có ${dem} số nguyên tố`;
};

//===================Bài 3============================

document.querySelector("#btnTinhTong3").onclick = function () {
  event.preventDefault();
  var nhapSo3 = document.querySelector("#nhapSo3").value * 1;
  var ketQua3 = 0;
  if (Number.isInteger(nhapSo3) && nhapSo3 > 2) {
    for (var i = 2; i <= nhapSo3; i++) {
      ketQua3 += i;
    }
  } else {
    alert("Vui lòng nhập số nguyên lớn hơn 2");
    return;
  }
  ketQua3 += 2 * nhapSo3;
  document.querySelector("#ketQuaTinhTong3").innerHTML = ketQua3;
};

//======================Bài 4=========================
document.querySelector("#btnTimUoc4").onclick = function () {
  event.preventDefault();
  var nhapSo4 = document.querySelector("#nhapSo4").value * 1;
  var ketQua4 = timUoc(nhapSo4);
  document.querySelector("#ketQuaTimUoc4").innerHTML = ketQua4;
};
//=====================Bài 5=========================

document.querySelector("#btnDaoNguoc5").onclick = function () {
  event.preventDefault();
  var ketQua5 = daoNguocChuoi("#nhapSo5");
  document.querySelector("#ketQuaDaoNguoc5").innerHTML = ketQua5;
};
//===================Bài 6==========================
document.querySelector("#btnTimX6").onclick = function () {
  event.preventDefault();
  var nhapSo6 = document.querySelector("#nhapSo6").value * 1;
  if (nhapSo6 >= 3) {
    var ketQua6 = timX(nhapSo6);
  } else {
    alert("Vui lòng nhập n lớn hơn 3");
    return;
  }
  document.querySelector("#ketQuaTimX6").innerHTML = ketQua6;
};
//=====================Bài 7====================
document.querySelector("#btnTinhBangCuuChuong7").onclick = function () {
  event.preventDefault();
  var nhapSo7 = document.querySelector("#nhapSo7").value * 1;
  var ketQua7 = tinhBCC(nhapSo7);
  document.querySelector("#ketQuaBangCuuChuong7").innerHTML = ketQua7;
};
//====================Bài 8==============================
var card = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
document.querySelector("#ketQuaXaoBai8").innerHTML = card;
document.querySelector("#btnXoaBai8").onclick = function () {
  ranDom(card);
  document.querySelector("#ketQuaXaoBai8").innerHTML = card;
};
document.querySelector("#btnChiaBai8").onclick = function () {
  var ketQua8 = "";
  var p1 = [];
  var p2 = [];
  var p3 = [];
  var p4 = [];
  for (var i = 0; i < card.length; i += 4) {
    p1.push(card[i]);
  }
  for (var i = 1; i < card.length; i += 4) {
    p2.push(card[i]);
  }
  for (var i = 2; i < card.length; i += 4) {
    p3.push(card[i]);
  }
  for (var i = 3; i < card.length; i += 4) {
    p4.push(card[i]);
  }
  ketQua8 = `<br> player1 = [${p1}] <br>player2 = [${p2}] <br>player3 = [${p3}] <br>player4 = [${p4}]`;
  document.querySelector("#ketQuaChiaBai8").innerHTML = ketQua8;
};
//=====================Bài 9=====================
document.querySelector("#btnTim9").onclick = function () {
  event.preventDefault();
  var nhapM = document.querySelector("#nhapSoConm9").value * 1;
  var nhapN = document.querySelector("#nhapSoChann9").value * 1;
  var ketQua9 = 0;
  var soGa = (4 * nhapM - nhapN) / 2;
  var soCho = 0;
  for (var i = 0; i <= nhapM; i++) {
    if (i == soGa) {
      soGa = i;
      break;
    }
  }
  soCho = nhapM - soGa;
  document.querySelector(
    "#ketQuaSoGaSoCho9"
  ).innerHTML = `Có ${soGa} con gà và ${soCho} con chó `;
};
//=====================Bài 10=====================
document.querySelector("#btnTimGoc10").onclick = function () {
  event.preventDefault();
  var ketQua10 = 0;
  var nhapGio = document.querySelector("#nhapGio10").value * 1;
  var nhapPhut = document.querySelector("#nhapPhut10").value * 1;
  if (nhapGio >= 0 && nhapGio <= 24 && Number.isInteger(nhapGio)) {
    if (nhapGio == 24) {
      nhapGio = 0;
    } else if (nhapGio > 11) {
      nhapGio = nhapGio - 12;
    } else {
      nhapGio = nhapGio;
    }
  } else {
    alert("vui lòng nhập số giờ và số phút đúng quy định");
    return;
  }

  console.log(nhapGio);
  var dieuKien =
    nhapGio >= 0 &&
    nhapGio <= 24 &&
    nhapPhut >= 0 &&
    nhapPhut <= 59 &&
    Number.isInteger(nhapGio) &&
    Number.isInteger(nhapPhut);
  nhapGio = nhapGio * 60;
  var thoiGian = nhapGio + nhapPhut;
  var gocKimGio = thoiGian * 0.5;
  var gocKimPhut = nhapPhut * 6;
  if (dieuKien) {
    ketQua10 = Math.abs(gocKimGio - gocKimPhut);
  } else {
    alert("vui lòng nhập số giờ và số phút đúng quy định");
    return;
  }
  if (ketQua10 > 180) {
    ketQua10 = 360 - ketQua10;
  }
  document.querySelector(
    "#ketQuaGoc10"
  ).innerHTML = `Vậy góc lệch giữa kim giờ và kim phút là ${ketQua10} độ`;
};
