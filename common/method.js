// Thêm Số vào 1 mảng
function themSo(idBtn, idInput, idform, nameArr, idKetQua) {
  document.querySelector(idBtn).onclick = function () {
    event.preventDefault();
    var nhapSo = document.querySelector(idInput).value;
    if (nhapSo == "") {
      return;
    }
    document.querySelector(idform).reset();
    nameArr.push(nhapSo * 1);
    document.querySelector(idKetQua).innerHTML = nameArr;
  };
}
// Xóa số khỏi 1 mảng
function xoaSo(idBtn, nameArr, idKetQua) {
  document.querySelector(idBtn).onclick = function () {
    nameArr.pop();
    document.querySelector(idKetQua).innerHTML = nameArr;
  };
}
//kiểm tra có phải số nguyên tố k
function soNguyenTo(sNT) {
  var dem = 0;
  for (var i = 1; i <= sNT; i++) {
    if (sNT % i == 0) {
      dem++;
    }
  }
  if (dem == 2) {
    var ketQua = sNT;
  }
  return ketQua;
}

// Tìm ước của 1 số
function timUoc(iSo) {
  var ketQua = "";
  for (var i = 1; i <= iSo; i++) {
    var uocSo = "";
    if (Number.isInteger(iSo / i)) {
      uocSo = i;
    }
    if (uocSo == iSo) {
      ketQua += `${uocSo} `;
    } else if (uocSo == i) {
      ketQua += `${uocSo} , `;
    }
  }
  return ketQua;
}
//Đảo ngược 1 chuỗi
function daoNguocChuoi(idInput) {
  var nhapChuoi = document.querySelector(idInput).value;
  var chuoiCoppy = "";
  for (var i = nhapChuoi.length - 1; i >= 0; i--) {
    var lay = nhapChuoi.slice(i, i + 1);
    chuoiCoppy += lay;
  }
  return chuoiCoppy;
}
// tìm x VD :  1 + 2 +3 + ... <= iSo

function timX(iSo) {
  var tong = 0;
  for (var i = 1; tong <= iSo; i++) {
    var tongThe = tong;
    if (tongThe + i > iSo) {
      break;
    } else {
      tong += i;
    }
  }
  return --i;
}

//Tính bảng cứu chương
function tinhBCC(iSo) {
  var ketQua1 = "";
  for (var i = 0; i <= 10; i++) {
    var ketQua2 = `${iSo} x ${i} = ` + iSo * i;
    ketQua1 += ketQua2 + `<br>`;
  }
  return ketQua1;
}
//RanDom phần tử trong mảng
function ranDom(arr) {
  arr.sort(function () {
    return 0.5 - Math.random();
  });
  return arr;
}
